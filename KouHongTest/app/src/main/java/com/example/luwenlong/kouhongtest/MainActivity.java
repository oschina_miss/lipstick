package com.example.luwenlong.kouhongtest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private LipstickGameView lipstickGameView;
    /**
     * X 10
     */
    private TextView sumTv;
    /**
     * 开始游戏
     */
    private Button start;
    /**
     * 发射💄
     */
    private Button launch;
    /**
     * x1
     */
    private TextView guanQiaTv;

    private int guanQia = 1;

    private static final int lipstickSum[] = {4, 5, 6};

    private static final int rotateSpeed_1[] = {-2, 2};

    private static final int rotateSpeed_2[] = {-2, -1, 1, 2};
    private SeekBar seekBar;
    /**
     * 当前30
     */
    private TextView speedTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || KeyEvent.KEYCODE_HOME == keyCode) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initView() {
        lipstickGameView = findViewById(R.id.lipstickGameView);
        lipstickGameView.setLipstickSum(lipstickSum[0]);
        lipstickGameView.setRotationSpeed(2);
        lipstickGameView.setLipstickGameListener(listener);
        seekBar = findViewById(R.id.seekBar);
        speedTv = findViewById(R.id.speedTv);

        guanQiaTv = findViewById(R.id.guanQiaTv);
        sumTv = findViewById(R.id.sumTv);
        start = findViewById(R.id.start);
        launch = findViewById(R.id.launch);
        launch.setEnabled(false);

        start.setOnClickListener(this);
        launch.setOnClickListener(this);

        sumTv.setText("x" + lipstickSum[0]);
        guanQiaTv.setText("1");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                speedTv.setText("当前"+progress);
                lipstickGameView.setLipstickLaunchSpeed(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 100) {
                if (guanQia == 2) {
                    int index = (int) (Math.random() * rotateSpeed_1.length);
                    lipstickGameView.setRotationSpeed(rotateSpeed_1[index]);
                } else if (guanQia == 3) {
                    int index = (int) (Math.random() * rotateSpeed_2.length);
                    lipstickGameView.setRotationSpeed(rotateSpeed_2[index]);
                }
                handler.sendEmptyMessageDelayed(100, 2000);
            }
            return false;
        }
    });


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start:
                lipstickGameView.start();
                break;
            case R.id.launch:
                lipstickGameView.launchLipstick();
                break;
        }
    }

    private LipstickGameListener listener = new LipstickGameListener() {

        @Override
        public void onStart() {
            start.setEnabled(false);
            launch.setEnabled(true);
            if (guanQia > 1) {
                handler.removeMessages(100);
                handler.sendEmptyMessageDelayed(100, 1000);
            }
        }

        @Override
        public void onStop() {
            start.setEnabled(true);
            launch.setEnabled(false);
        }

        @Override
        public void onLipstick(int sum) {
            sumTv.setText("x" + sum);
        }

        @Override
        public void onSuccess() {
            if (guanQia == 3) {
                successDialog2();
            } else {
                successDialog();
            }
            lipstickGameView.setRotationSpeed(2);
            handler.removeMessages(100);
        }

        @Override
        public void onFailure() {
            guanQia = 1;
            errorDialog();
            lipstickGameView.setRotationSpeed(2);
            handler.removeMessages(100);
        }
    };


    private void successDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
        dialogBuilder
                .withTitle("成功")
                .withMessage("是否开始下一关？")
                .withButton1Text("是")
                .withButton2Text("否")
                .withDialogColor(getResources().getColor(R.color.color_009966))
                .isCancelableOnTouchOutside(false)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQia = guanQia + 1;
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[guanQia - 1]);
                        lipstickGameView.reset();
                        lipstickGameView.start();
                        dialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQia = 1;
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[0]);
                        lipstickGameView.reset();
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }

    private void errorDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
        dialogBuilder
                .withTitle("失败")
                .withMessage("是否重新开始？")
                .withButton1Text("是")
                .withButton2Text("否")
                .withDialogColor(getResources().getColor(R.color.color_CC9909))
                .isCancelableOnTouchOutside(false)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[0]);
                        lipstickGameView.reset();
                        lipstickGameView.start();
                        dialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQia = 1;
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[0]);
                        lipstickGameView.reset();
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }

    private void successDialog2() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);
        dialogBuilder
                .withTitle("成功")
                .withMessage("😁")
                .withButton1Text("继续游戏")
                .withButton2Text("关闭")
                .withDialogColor(getResources().getColor(R.color.color_FF0033))
                .isCancelableOnTouchOutside(false)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQia = 1;
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[0]);
                        lipstickGameView.reset();
                        lipstickGameView.start();
                        dialogBuilder.dismiss();
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        guanQia = 1;
                        guanQiaTv.setText(guanQia + "");
                        lipstickGameView.setLipstickSum(lipstickSum[0]);
                        lipstickGameView.reset();
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }

}
