package com.example.luwenlong.kouhongtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Path;

/**
 * Date: 2018/12/5
 * <p>
 * Time: 10:45 PM
 * <p>
 * author: 鹿文龙
 */
public class Lipstick {

    public Bitmap bitmap;

    public Matrix matrix = new Matrix();

    public Path path = new Path();

    /**
     * 口红坐标
     */
    public float dx;

    public float dy;

    /**
     * 口红最高点坐标
     */
    public float topDx;

    public float topDy;
    /**
     * 发射口红最终坐标
     */
    public float finalLipstickTopCy;

    /**
     * 口红所在4个顶点坐标
     * <p>
     * 0，1 左边最高点  x,y
     * 2，3 右边最高点  x,y
     * 4，5 左边最低点  x,y
     * 6，7 右边最低点  x,y
     */
    public float movePoints[] = new float[8];
    /**
     * 旋转后新的顶点坐标
     */
    public float rotatePoints[] = new float[8];

    /**
     * 旋转角度
     */
    private int angle;

    /**
     * 旋转速度
     */
    private int rotateSpeed;

    public Lipstick(Context context, int imageRes, int width, int height, int viewWidth, int viewheight) {
        this.bitmap = BitmapFactory.decodeResource(context.getResources(),imageRes);
        this.bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        this.dx = (viewWidth - width) / 2;
        this.dy = viewheight - height;
        this.topDx = viewWidth / 2;
        this.topDy = dy;
        this.finalLipstickTopCy = dy;
    }

    /**
     * 设置转速
     * @param speed
     */
    public void setRotateSpeed(int speed) {
        this.rotateSpeed = speed;
    }

    /**
     * 设置💄四个顶点坐标
     */
    public void setLipstickCoordinates(float[] lipstickValues) {
        movePoints[0] = lipstickValues[Matrix.MTRANS_X];
        movePoints[1] = lipstickValues[Matrix.MTRANS_Y];
        movePoints[2] = lipstickValues[Matrix.MTRANS_X] + bitmap.getWidth();
        movePoints[3] = lipstickValues[Matrix.MTRANS_Y];
        movePoints[4] = lipstickValues[Matrix.MTRANS_X];
        movePoints[5] = lipstickValues[Matrix.MTRANS_Y] + bitmap.getHeight();
        movePoints[6] = lipstickValues[Matrix.MTRANS_X] + bitmap.getWidth();
        movePoints[7] = lipstickValues[Matrix.MTRANS_Y] + bitmap.getHeight();
    }

    /**
     * 更新顶点坐标
     *
     * @param disk
     */
    public void updateLipstickCoordinates(Disk disk) {
        angle = angle % 360 + rotateSpeed;
        rotatePoints[0] = getX(movePoints[0], movePoints[1], disk);
        rotatePoints[1] = getY(movePoints[0], movePoints[1], disk);
        rotatePoints[2] = getX(movePoints[2], movePoints[3], disk);
        rotatePoints[3] = getY(movePoints[2], movePoints[3], disk);
        rotatePoints[4] = getX(movePoints[4], movePoints[5], disk);
        rotatePoints[5] = getY(movePoints[4], movePoints[5], disk);
        rotatePoints[6] = getX(movePoints[6], movePoints[7], disk);
        rotatePoints[7] = getY(movePoints[6], movePoints[7], disk);
    }


    /**
     * 假设对图片上任意点(x,y)，绕一个坐标点(rx0,ry0)逆时针旋转a角度后的新的坐标设为(x0, y0)，有公式
     * <p>
     * x0= (x - rx0)*cos(a) - (y - ry0)*sin(a) + rx0 ;
     * y0= (x - rx0)*sin(a) + (y - ry0)*cos(a) + ry0 ;
     *
     * @param x
     * @param y
     * @return
     */
    private float getX(float x, float y, Disk disk) {
        return (float) ((x - disk.cx) * Math.cos(Math.PI / 180 * angle) - (y - disk.cy) * Math.sin(Math.PI / 180 * angle) + disk.cx);
    }

    private float getY(float x, float y, Disk disk) {
        return (float) ((y - disk.cy) * Math.cos(Math.PI / 180 * angle) + (x - disk.cx) * Math.sin(Math.PI / 180 * angle) + disk.cy);
    }

}
