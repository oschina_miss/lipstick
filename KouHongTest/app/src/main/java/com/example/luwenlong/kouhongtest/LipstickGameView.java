package com.example.luwenlong.kouhongtest;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 2018/11/28
 * <p>
 * Time: 10:44 PM
 * <p>
 * author: 鹿文龙
 */
public class LipstickGameView extends SurfaceView implements SurfaceHolder.Callback, Handler.Callback {

    /**
     * 创建
     */
    private final static int WHAT_CREATE = 1;
    /**
     * 循环
     */
    private final static int WHAT_DISK_ROTATE = 2;
    /**
     * 💄移动
     */
    private final static int WHAT_LIPSTICK_MOVE = 3;

    /**
     * 爆炸动画
     */
    private final static int WHAT_BOOM = 4;
    /**
     * 💿大小
     */
    private final static int DISK_SIZE = 400;
    /**
     * 💄大小
     */
    private final static int LIPSTICK_WIDTH = 46;
    private final static int LIPSTICK_HEIGHT = 200;
    /**
     * 💄发射速度
     */
    private final static int LIPSTICK_LAUNCH_SPEED = 10;

    private SurfaceHolder holder;

    private Handler handler;

    private int diskImage;             //💿图片
    private int diskSize;              //💿盘大小
    private int lipstickImage;         //💄图片
    private int lipstickWidth;         //💄宽度
    private int lipstickHeight;        //💄高度
    private int lipstickLaunchSpeed;   //💄发射速度

    private Paint paint = new Paint(); //画笔
    private Paint boomPaint = new Paint(); //画笔
    private Disk disk;                 //💿
    private Lipstick lipstick;         //当前💄
    private Lipstick bottomLipstick;   //底部固定的💄

    private int rotateSpeed = 2;       //💿转速速度
    private int lipstickSum = 6;       //💄数量
    private int finalLipstickSum = 6;  //💄数量(最终的)

    private float[] lipstickValues = new float[9];//当前💄的矩阵

    private boolean isLaunchLipstick;   //是否发射💄

    private boolean lipstickWithDisk;   //💄和💿是否相撞了

    private boolean isStart;            //开始游戏
    private boolean isSuccess;          //成功
    private boolean isFailure;          //失败

    private ValueAnimator valueAnimator;
    //动画持续时间
    public int duration = 1000;
    //所有粒子
    private Particle[][] mParticles;


    private LipstickGameListener lipstickGameListener;

    //插中💄的旋转列表
    private List<Lipstick> lipsticks = new ArrayList<>();

    public LipstickGameView(Context context) {
        super(context);
        init(null);
    }

    public LipstickGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public LipstickGameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.LipstickGameView);
        diskImage = ta.getResourceId(R.styleable.LipstickGameView_disk_image, R.drawable.ic_zhuanpan);
        diskSize = ta.getInteger(R.styleable.LipstickGameView_disk_size, DISK_SIZE);

        lipstickImage = ta.getResourceId(R.styleable.LipstickGameView_lipstick_image, R.drawable.ic_kouhon);
        lipstickWidth = ta.getInteger(R.styleable.LipstickGameView_lipstick_width, LIPSTICK_WIDTH);
        lipstickHeight = ta.getInteger(R.styleable.LipstickGameView_lipstick_height, LIPSTICK_HEIGHT);
        lipstickLaunchSpeed = -ta.getInteger(R.styleable.LipstickGameView_lipstick_launch_speed, LIPSTICK_LAUNCH_SPEED);
        //设置是否使用抗锯齿功能，会消耗较大资源，绘制图形速度会变慢。
        paint.setAntiAlias(true);
        //设定是否使用图像抖动处理，会使绘制出来的图片颜色更加平滑和饱满，图像更加清晰
        paint.setDither(true);

        //设置是否使用抗锯齿功能，会消耗较大资源，绘制图形速度会变慢。
        boomPaint.setAntiAlias(true);
        //设定是否使用图像抖动处理，会使绘制出来的图片颜色更加平滑和饱满，图像更加清晰
        boomPaint.setDither(true);

        getHolder().addCallback(this);
    }

    /**
     * 开始游戏
     */
    public void start() {
        if (!isStart) {
            isStart = true;
            isSuccess = false;
            isFailure = false;
            lipsticks.clear();
            handler.sendEmptyMessage(WHAT_DISK_ROTATE);
            if (null != lipstickGameListener) {
                lipstickGameListener.onStart();
            }
        }
    }

    /**
     * 停止游戏
     */
    private void stop(boolean isSuccess) {
        handler.removeMessages(WHAT_CREATE);
        handler.removeMessages(WHAT_DISK_ROTATE);
        handler.removeMessages(WHAT_LIPSTICK_MOVE);
        handler.removeMessages(WHAT_BOOM);
        isStart = false;
        if (null != lipstickGameListener) {
            if (isSuccess) {
                lipstickGameListener.onSuccess();
            } else {
                lipstickGameListener.onFailure();
            }
            lipstickGameListener.onStop();
        }
    }

    /**
     * 重置画布
     */
    public void reset() {
        isStart = false;
        isSuccess = false;
        isFailure = false;
        lipsticks.clear();
        lipstickSum = finalLipstickSum;
        handler.sendEmptyMessage(WHAT_CREATE);
        handler.removeMessages(WHAT_BOOM);
        if (null != lipstickGameListener) {
            lipstickGameListener.onLipstick(finalLipstickSum);
        }
        if (null != valueAnimator) {
            valueAnimator.cancel();
        }
    }

    /**
     * 发射💄
     */
    public void launchLipstick() {
        if (isStart && lipstickSum > 0) {
            isLaunchLipstick = true;
            handler.removeMessages(WHAT_DISK_ROTATE);
            handler.sendEmptyMessage(WHAT_LIPSTICK_MOVE);
            if (null != lipstickGameListener) {
                lipstickSum--;
                lipstickGameListener.onLipstick(lipstickSum);
            }
        }
    }

    /**
     * 设置💄数量
     *
     * @param sum
     */
    public void setLipstickSum(int sum) {
        lipstickSum = sum;
        finalLipstickSum = sum;
        if (null != lipstickGameListener) {
            lipstickGameListener.onLipstick(sum);
        }
    }

    /**
     * 💄发射速度
     *
     * @param lipstickLaunchSpeed
     */
    public void setLipstickLaunchSpeed(int lipstickLaunchSpeed) {
        this.lipstickLaunchSpeed = -lipstickLaunchSpeed;
    }

    /**
     * 设置转速
     *
     * @param speed
     */
    public void setRotationSpeed(int speed) {
        rotateSpeed = speed;
    }

    public void setLipstickGameListener(LipstickGameListener lipstickGameListener) {
        this.lipstickGameListener = lipstickGameListener;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.holder = holder;
        handler = new Handler(this);
        handler.sendEmptyMessage(WHAT_CREATE);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (null != disk.bitmap && !disk.bitmap.isRecycled()) {
            disk.bitmap.recycle();
            disk.bitmap = null;
        }
        if (null != lipstick.bitmap && !lipstick.bitmap.isRecycled()) {
            lipstick.bitmap.recycle();
            lipstick.bitmap = null;
        }

        for (Lipstick lipstick : lipsticks) {
            if (null != lipstick.bitmap && !lipstick.bitmap.isRecycled()) {
                lipstick.bitmap.recycle();
                lipstick.bitmap = null;
            }
        }
        System.gc();
    }

    @Override
    public boolean handleMessage(Message msg) {
        Canvas canvas = holder.lockCanvas();
        //绘制背景
        canvas.drawColor(Color.WHITE);
        switch (msg.what) {
            case WHAT_CREATE:
                disk = new Disk(getContext(), diskImage, diskSize, getWidth(), getHeight());
                //绘制💿,初始化位置
                disk.matrix.postTranslate(disk.dx, disk.dy);
                canvas.drawBitmap(disk.bitmap, disk.matrix, paint);
                //绘制可移动的💄
                drawLipstick(canvas);
                //初始化底部💄
                bottomLipstick = new Lipstick(getContext(), lipstickImage, lipstickWidth, lipstickHeight, getWidth(), getHeight());
                bottomLipstick.matrix.postTranslate(bottomLipstick.dx, bottomLipstick.dy);
                break;
            case WHAT_DISK_ROTATE:
                //旋转💿
                drawRotateDisk(canvas);
                handler.sendEmptyMessage(WHAT_DISK_ROTATE);
                break;
            case WHAT_LIPSTICK_MOVE:
                //如果插中了，则💄停止移动,并且💿继续转动
                lipstickWithDisk = isCollisionWithDisk(lipstick.topDx, lipstick.finalLipstickTopCy);
                if (lipstickSum == 0 && lipstickWithDisk) {
                    lipstick.matrix.preTranslate(0, lipstickLaunchSpeed);
                    canvas.drawBitmap(lipstick.bitmap, lipstick.matrix, paint);
                    isSuccess = true;
                    stop(true);
                } else {
                    if (lipstickWithDisk) {
                        lipstick.finalLipstickTopCy = lipstick.topDx;
                        lipsticks.add(lipstick);
                        //创建一个新的💄
                        drawLipstick(canvas);
                        isLaunchLipstick = false;
                    } else if (isLaunchLipstick) {
                        //平移💄
                        lipstick.matrix.preTranslate(0, lipstickLaunchSpeed);
                        canvas.drawBitmap(lipstick.bitmap, lipstick.matrix, paint);
                        lipstick.finalLipstickTopCy += lipstickLaunchSpeed;
                        lipstick.matrix.getValues(lipstickValues);
                        lipstick.setLipstickCoordinates(lipstickValues);
                    }
                }
                drawRotateLipstick(canvas);
                drawRotateDisk(canvas);
                break;
            case WHAT_BOOM:
                drawParticle(canvas);
                drawRotateLipstick(canvas);
                canvas.drawBitmap(disk.bitmap, disk.matrix, paint);
                break;
        }
        //绘制底部💄
        canvas.drawBitmap(bottomLipstick.bitmap, bottomLipstick.matrix, paint);
        //解锁画布，提交画好的图像
        holder.unlockCanvasAndPost(canvas);
        return false;
    }

    /**
     * 绘制旋转💿
     *
     * @param canvas
     */
    private void drawRotateDisk(Canvas canvas) {
        disk.matrix.postRotate(rotateSpeed, disk.cx, disk.cy);
        canvas.drawBitmap(disk.bitmap, disk.matrix, paint);
    }

    /**
     * 绘制💄
     *
     * @param canvas
     */
    private void drawLipstick(Canvas canvas) {
        lipstick = new Lipstick(getContext(), lipstickImage, lipstickWidth, lipstickHeight, getWidth(), getHeight());
        lipstick.matrix.postTranslate(lipstick.dx, lipstick.dy);
        canvas.drawBitmap(lipstick.bitmap, lipstick.matrix, paint);
    }

    private void drawRotateLipstick(Canvas canvas) {
        if (isFailure) {
            for (Lipstick lipstick : lipsticks) {
                canvas.drawBitmap(lipstick.bitmap, lipstick.matrix, paint);
            }
        } else {
            for (Lipstick lipstick : lipsticks) {
                lipstick.setRotateSpeed(rotateSpeed);
                lipstick.matrix.postRotate(rotateSpeed, disk.cx, disk.cy);
                canvas.drawBitmap(lipstick.bitmap, lipstick.matrix, paint);
                lipstick.updateLipstickCoordinates(disk);
                if (isCollisionWithLipstick(lipstick) && !isFailure) {
                    isFailure = true;
                }
            }

            if (isFailure) {
                boom();
                stop(false);
            }

            if (!isFailure && !isSuccess) {
                handler.sendEmptyMessage(WHAT_LIPSTICK_MOVE);
            }
        }
    }

    /**
     * 💥
     */
    private void boom() {
        Rect rect = new Rect((int) lipstickValues[Matrix.MTRANS_X], (int) lipstickValues[Matrix.MTRANS_Y], (int) (lipstickValues[Matrix.MTRANS_X] + lipstickWidth), (int) (lipstickValues[Matrix.MTRANS_Y] + lipstickHeight));
        mParticles = Particle.generateParticles(lipstick.bitmap, rect);
        valueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
        valueAnimator.setDuration(duration);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                handler.sendEmptyMessage(WHAT_BOOM);
            }
        });
        valueAnimator.start();
    }

    /**
     * 绘制💥动画
     *
     * @param canvas
     */
    private void drawParticle(Canvas canvas) {
        for (Particle[] particle : mParticles) {
            for (Particle p : particle) {
                p.update((Float) valueAnimator.getAnimatedValue());
                boomPaint.setColor(p.color);
                boomPaint.setAlpha((int) (Color.alpha(p.color) * p.alpha));
                canvas.drawCircle(p.cx, p.cy, p.radius, boomPaint);//
            }
        }
    }

    /**
     * 💄和💿是否发生碰撞
     *
     * @param x
     * @param y
     * @return
     */
    private boolean isCollisionWithDisk(float x, float y) {
        // 如果点和圆心距离小于或等于半径则认为发生碰撞
        return Math.sqrt(Math.pow(x - disk.cx, 2) + Math.pow(y - disk.cy, 2)) <= disk.r;
    }

    /**
     * 💄是否碰撞
     *
     * @param mLipstick
     * @return
     */
    private boolean isCollisionWithLipstick(Lipstick mLipstick) {

        boolean b1 = eLineAtELine(mLipstick.rotatePoints[0], mLipstick.rotatePoints[1], mLipstick.rotatePoints[2], mLipstick.rotatePoints[3], lipstick.movePoints[0], lipstick.movePoints[1], lipstick.movePoints[2], lipstick.movePoints[3]);

        boolean b2 = eLineAtELine(mLipstick.rotatePoints[2], mLipstick.rotatePoints[3], mLipstick.rotatePoints[4], mLipstick.rotatePoints[5], lipstick.movePoints[0], lipstick.movePoints[1], lipstick.movePoints[2], lipstick.movePoints[3]);

        boolean b3 = eLineAtELine(mLipstick.rotatePoints[4], mLipstick.rotatePoints[5], mLipstick.rotatePoints[6], mLipstick.rotatePoints[7], lipstick.movePoints[0], lipstick.movePoints[1], lipstick.movePoints[2], lipstick.movePoints[3]);

        boolean b4 = eLineAtELine(mLipstick.rotatePoints[0], mLipstick.rotatePoints[1], mLipstick.rotatePoints[6], mLipstick.rotatePoints[7], lipstick.movePoints[0], lipstick.movePoints[1], lipstick.movePoints[2], lipstick.movePoints[3]);

        return b1 || b2 || b3 || b4;
    }

    /**
     * 描述：线段与线段相交.
     * 点A（x1，y1）,B(x2,y2),C(x3,y3),D(x4,y4)
     * 线段AB与线段CD相交吗?
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     * @param x4
     * @param y4
     * @return
     */
    private boolean eLineAtELine(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {

        if (x3 > 0) {
            x3 = x3 + 50;
        }

        if (x4 > 0) {
            x4 = x4 - 50;
        }

        double k1 = (y2 - y1) / (x2 - x1);
        double k2 = (y4 - y3) / (x4 - x3);
        if (k1 == k2) {
            return false;
        } else {
            double x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x3 * y4 - y3 * x4) * (x1 - x2)) / ((y2 - y1) * (x3 - x4) - (y4 - y3) * (x1 - x2));
            double y = (x1 * y2 - y1 * x2 - x * (y2 - y1)) / (x1 - x2);
            //System.out.println("直线的交点("+x+","+y+")");
            if (x >= Math.min(x1, x2) && x <= Math.max(x1, x2)
                    && y >= Math.min(y1, y2) && y <= Math.max(y1, y2)
                    && x >= Math.min(x3, x4) && x <= Math.max(x3, x4)
                    && y >= Math.min(y3, y4) && y <= Math.max(y3, y4)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
