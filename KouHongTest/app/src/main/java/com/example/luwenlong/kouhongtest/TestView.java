package com.example.luwenlong.kouhongtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Arrays;

/**
 * Date: 2018/12/5
 * <p>
 * Time: 4:51 PM
 * <p>
 * author: 鹿文龙
 */
public class TestView extends SurfaceView implements SurfaceHolder.Callback, Handler.Callback {

    private Paint paint = new Paint();

    private Handler handler;

    private SurfaceHolder holder;

    private Matrix matrix = new Matrix();

    private float[] values = new float[9];

    public TestView(Context context) {
        super(context);
        init();
    }

    public TestView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TestView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint.setAntiAlias(true);
        paint.setStrokeWidth(15);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(getContext().getResources().getColor(R.color.color_CC9909));
        getHolder().addCallback(this);
    }

    public void start() {
        handler.sendEmptyMessage(200);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.holder = holder;
        handler = new Handler(this);
        handler.sendEmptyMessage(100);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private int degrees;

    private int speed = -1;

    private int diskCx;

    private int diskCy;


    @Override
    public boolean handleMessage(Message msg) {
        Canvas canvas = holder.lockCanvas();
        diskCx = getWidth() / 2;
        diskCy = 500;
        canvas.drawColor(Color.WHITE);
        switch (msg.what) {
            case 200:
                handler.sendEmptyMessage(200);
                degrees = degrees % 360 + speed;
                break;
        }
        paint.setColor(getContext().getResources().getColor(R.color.color_CC9909));
        canvas.drawRect(diskCx - 25, 700, diskCx + 25, 900, paint);
        canvas.drawCircle(diskCx, diskCy, 200, paint);
        canvas.drawCircle(diskCx, diskCy, 5, paint);

        holder.unlockCanvasAndPost(canvas);
        return false;
    }

    /**
     * 描述：线段与线段相交.
     * 点A（x1，y1）,B(x2,y2),C(x3,y3),D(x4,y4)
     * 线段AB与线段CD相交吗?
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     * @param x4
     * @param y4
     * @return
     */
    public static boolean eLineAtELine(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
        double k1 = (y2 - y1) / (x2 - x1);
        double k2 = (y4 - y3) / (x4 - x3);
        if (k1 == k2) {
            return false;
        } else {
            double x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x3 * y4 - y3 * x4) * (x1 - x2)) / ((y2 - y1) * (x3 - x4) - (y4 - y3) * (x1 - x2));
            double y = (x1 * y2 - y1 * x2 - x * (y2 - y1)) / (x1 - x2);
            //System.out.println("直线的交点("+x+","+y+")");
            if (x >= Math.min(x1, x2) && x <= Math.max(x1, x2)
                    && y >= Math.min(y1, y2) && y <= Math.max(y1, y2)
                    && x >= Math.min(x3, x4) && x <= Math.max(x3, x4)
                    && y >= Math.min(y3, y4) && y <= Math.max(y3, y4)) {
                return true;
            } else {
                return false;
            }
        }
    }


}
