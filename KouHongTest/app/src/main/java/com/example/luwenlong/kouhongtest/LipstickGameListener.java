package com.example.luwenlong.kouhongtest;

/**
 * Date: 2018/12/7
 * <p>
 * Time: 12:04 PM
 * <p>
 * author: 鹿文龙
 */
public interface LipstickGameListener {

    void onStart();

    void onStop();

    void onLipstick(int sum);

    void onSuccess();

    void onFailure();

}
