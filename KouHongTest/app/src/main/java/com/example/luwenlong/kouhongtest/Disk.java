package com.example.luwenlong.kouhongtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

/**
 * Date: 2018/12/5
 * <p>
 * Time: 11:20 PM
 * <p>
 * author: 鹿文龙
 */
public class Disk {

    //创建矩阵控制图片旋转和平移
    public Matrix matrix = new Matrix();
    //圆盘
    public Bitmap bitmap;
    //圆盘横坐标
    public int dx;
    //圆盘纵坐标
    public int dy;
    //圆盘圆心,半径
    public int cx;
    public int cy;
    public int r;

    public Disk(Context context, int imageRes, int size, int viewWidth, int viewheight) {
        bitmap = BitmapFactory.decodeResource(context.getResources(), imageRes);
        bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
        dx = (viewWidth - size) / 2;
        dy = viewheight / 4;
        cx = viewWidth / 2;
        cy = dy + size / 2;
        r = size / 2;
    }

}
